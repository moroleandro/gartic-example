const express = require('express');
const socketIo = require('socket.io');
const http = require('http');
const ipServer = require('ip');

const app = express();
const server = http.createServer(app);
const io = socketIo.listen(server);

server.listen(3000, () => {
    console.log("> Server Running - Host: http://" + ipServer.address() +":3000");
    console.log("");
});

app.use(express.static(__dirname + "/canvas"));

const historyLine = [];
const clientsHosts = [];

io.on('connection', (socket) => {
    socket.on('ipclient', (ip) => {
        if(!clientsHosts.find((clientsHosts) => clientsHosts === ip)){
            clientsHosts.push(ip);
            console.log('-- New connection | IP: ' + ip + " --");
            console.log('-- Total connections at the time: ' + clientsHosts.length + " --");
        }
    });
    
    historyLine.forEach(line => {
        socket.emit('write', line);
    });

    socket.on('write', (line) => {
        historyLine.push(line);
        io.emit('write', line);
    });

    socket.on('erase', (line) => {
        historyLine.push(line);
        io.emit('erase', line);
    });
});

