document.addEventListener('DOMContentLoaded', () => {

    const socket = io.connect();
    const btnEraseScreen = document.querySelector('#btnErase');
    
    // Emit host client
    getIp(function(ip){
        socket.emit('ipclient', ip);
    });
    
    
    // Config pen and screen
    const pen = {
        active: false,
        moved: false,
        position: { x:0, y:0},
        positionPrev: null
    }

    const screen = document.querySelector('#screen');
    const context = screen.getContext('2d');
    screen.width = 700;
    screen.height = 500;

    context.lineWidth = 4;
    context.strokeStyle = "red"

    const writeLine = (line) => {
        context.beginPath();
        context.moveTo(line.position.x,line.position.y);
        context.lineTo(line.positionPrev.x,line.positionPrev.y);
        context.stroke();
    }

    screen.onmousedown = (event) => {pen.active = true}
    screen.onmouseup = (event) => {pen.active = false}
    screen.onmousemove = (event) => {
        pen.position.x = event.clientX;
        pen.position.y = event.clientY;
        pen.moved = true;
    }

    // Clear screen
    btnEraseScreen.onclick = () => {
        socket.emit('erase', {position: 0, positionPrev: 0});
        pen.moved = false;
    };

    socket.on('erase', (line) => {
        context.clearRect(0, 0, screen.width, screen.height);
        pen.active = false;
        pen.moved = false;
        pen.position = line;
        pen.positionPrev = null;
    });

    // Received message of write on the screen
    socket.on('write', (line) => {
        writeLine(line);
    });

    // Emit event clients
    const cicle = () => {
        if(pen.active && pen.moved && pen.positionPrev){
            socket.emit('write', {position: pen.position, positionPrev: pen.positionPrev})
            //writeLine({position: pen.position, positionPrev: pen.positionPrev});
            pen.moved = false;
        }
        pen.positionPrev = {x: pen.position.x, y: pen.position.y}

        setTimeout(cicle, 5);
    }

    cicle();
});

function getIp(callback)
{
    function response(s)
    {
        callback(window.userip);
        s.onload = s.onerror = null;
        document.body.removeChild(s);
    }

    function trigger()
    {
        window.userip = false;

        var s = document.createElement("script");
        s.async = true;
        s.onload = function() {
            response(s);
        };
        s.onerror = function() {
            response(s);
        };

        s.src = "https://l2.io/ip.js?var=userip";
        document.body.appendChild(s);
    }

    if (/^(interactive|complete)$/i.test(document.readyState)) {
        trigger();
    } else {
        document.addEventListener('DOMContentLoaded', trigger);
    }
}