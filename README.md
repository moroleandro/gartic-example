
<h3 align="center">
    <img alt="Logo" title="#logo" width="300px" src="https://gartic.com.br/imgs/v3/logoHome2x.png?v=1">
    <br/><br/>
    <b>Example game <a href="https://gartic.com.br/">Gartic</a></b> 
</h3><br/>

## Requirements
- NodeJS > 12
- npm or yarn

## Start

Clone this repository and execute the following commands

    $ npm run install
    $ npm run start

Automatically when you start the server will be presented on your terminal the server host and the new connections that are entering the game. Or you can access http://localhost:3000

## Contribute
This project is under construction. If you're interested, fork and send me :)

<h4 align="center">
    All rights reserved to <a href="https://www.linkedin.com/in/moroleandro/" target="_blank">Leandro Moro</a>
</h4>